/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include <asm/unistd.h>
#include <sched.h>
#include <signal.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>

// Functions implemented in morello_kernel_test_impl.S.
extern "C" {
  size_t morello_kernel_test_count_preserved_regs(void(*)());
  size_t morello_kernel_test_count_restore_sysregs();
  void morello_kernel_test_getpid();
  void morello_kernel_test_nanosleep();
  void morello_kernel_test_loop();
  void morello_kernel_test_alarm_handler(int, siginfo_t*, void*);
  void morello_kernel_test_c64_handler(int);
  void morello_kernel_test_fork();
}

// Number of C registers that morello_kernel_test_count_preserved_regs() sets and then counts after
// calling the function passed to it.
#ifndef SMALL_CAP_REGISTER_FILE
constexpr size_t kNbSetCRegs = 28;
#else
constexpr size_t kNbSetCRegs = 13;
#endif
// Number of system registers set by morello_kernel_test_fork().
constexpr size_t kNbSetSysregs = 6;

// Address of a page to be used by the fork test for testing copy-on-write behaviour.
void *morello_kernel_test_cow_page;

namespace {

// The signal handlers we use to test the kernel are not compatible with libshim, because they
// strongly assume that they are directly invoked by the kernel and directly return to the sigreturn
// trampoline (in the vDSO). This helper makes a raw sigaction syscall, bypassing libshim.
__attribute__((noinline))
int raw_sigaction(int signal, const struct __kernel_sigaction* new_action,
                  struct __kernel_sigaction* old_action) {
  register int ret_ asm("w0") = signal;
  register const struct __kernel_sigaction* new_action_ asm("x1") = new_action;
  register struct __kernel_sigaction* old_action_ asm("x2") = old_action;
  register size_t sigsetsize_ asm("x3") = sizeof(sigset_t);
  register int nr_ asm("w8") = __NR_rt_sigaction;

  asm("svc #0" : "+r"(ret_) : "r"(new_action_), "r"(old_action_), "r"(sigsetsize_), "r"(nr_));

  return ret_;
}

}

TEST(morello_kernel, getpid) {
  // Check that C registers are preserved when going back and forth into the kernel, i.e. after a
  // context switch to kernel mode (getpid has been chosen because it has no side effect and takes
  // no argument).
  ASSERT_EQ(morello_kernel_test_count_preserved_regs(&morello_kernel_test_getpid), kNbSetCRegs);
}

TEST(morello_kernel, nanosleep) {
  // Check that C registers are preserved when another process is scheduled (while this one is
  // sleeping), i.e. after a context switch to another process.
  ASSERT_EQ(morello_kernel_test_count_preserved_regs(&morello_kernel_test_nanosleep), kNbSetCRegs);
}

TEST(morello_kernel, signal) {
  // Set up a signal handler to be triggered in one second. Use an RT signal handler so that:
  // - The handler can change the saved PC through the ucontext pointer (see
  //   morello_kernel_test_alarm_handler()).
  // - We make sure that the kernel calls the signal handler with the arguments set up as expected
  //   (at least the ucontext pointer).
  struct __kernel_sigaction alarm_handler{};
  // The raw kernel sigaction struct only has a .sa_handler field, so we need to cast our
  // .sa_sigaction handler.
  alarm_handler.sa_handler = reinterpret_cast<__sighandler_t>(morello_kernel_test_alarm_handler);
  alarm_handler.sa_flags = SA_SIGINFO;
  ASSERT_EQ(raw_sigaction(SIGALRM, &alarm_handler, nullptr), 0);

  alarm(1);

  // Check that C registers are preserved when a signal handler is called, i.e. after a context
  // switch to a signal handler.
  // Additionally, check that the signal handler is able to change the saved PC in order to make the
  // interrupted code jump to another place (in this case, after the loop).
  ASSERT_EQ(morello_kernel_test_count_preserved_regs(&morello_kernel_test_loop), kNbSetCRegs);
}

TEST(morello_kernel, signal_c64) {
  // Set up a C64 signal handler (LSB set) for SIGUSR1 and send it to ourselves. If the handler
  // returns without any crash, this means that the kernel did the right thing (clear the LSB and
  // set PSTATE.C64 when invoking the handler).
  struct __kernel_sigaction usr1_handler{};
  usr1_handler.sa_handler = morello_kernel_test_c64_handler;
  ASSERT_EQ(raw_sigaction(SIGUSR1, &usr1_handler, nullptr), 0);
  raise(SIGUSR1);
}

TEST(morello_kernel, fork) {
  // Allocate a page to be used by morello_kernel_test_fork() (MAP_PRIVATE to get copy-on-write
  // behaviour).
  morello_kernel_test_cow_page = mmap(nullptr, PAGE_SIZE, PROT_READ | PROT_WRITE,
                                      MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  ASSERT_NE(morello_kernel_test_cow_page, MAP_FAILED);

  // We check that the kernel is context-switching registers as it should during the test.
  // To ensure that we catch potential bugs, we need to have both threads scheduled on the same CPU.
  cpu_set_t old_cpu_set, new_cpu_set;
  ASSERT_EQ(sched_getaffinity(0, sizeof(old_cpu_set), &old_cpu_set), 0);
  CPU_ZERO(&new_cpu_set);
  int cpu = sched_getcpu();
  ASSERT_NE(cpu, -1);
  CPU_SET(cpu, &new_cpu_set);
  ASSERT_EQ(sched_setaffinity(0, sizeof(new_cpu_set), &new_cpu_set), 0);

  // Fork a child and check that C registers are preserved (in this process, i.e. the parent) after
  // the fork is complete.
  EXPECT_EQ(morello_kernel_test_count_preserved_regs(&morello_kernel_test_fork), kNbSetCRegs);

  // Wait for the child to complete and check that it has exited properly.
  int status;
  pid_t child_pid = wait(&status);
  EXPECT_GT(child_pid, 0) << strerror(errno);
  EXPECT_TRUE(WIFEXITED(status));
  size_t ret_val = WEXITSTATUS(status);
  // The 5 lower bits contain the number of C registers and capabilities on the stack preserved
  // after a copy-on-write.
  EXPECT_EQ(ret_val & 0x1f, kNbSetCRegs);
  // The upper 3 bits contain the number of sysregs preserved.
  EXPECT_EQ(ret_val >> 5, kNbSetSysregs);

  // Make sure that the Morello system registers have not been corrupted by the child thread, and
  // restore them to their default value.
  EXPECT_EQ(morello_kernel_test_count_restore_sysregs(), kNbSetSysregs);

  // Restore CPU affinity.
  EXPECT_EQ(sched_setaffinity(0, sizeof(old_cpu_set), &old_cpu_set), 0);

  munmap(morello_kernel_test_cow_page, PAGE_SIZE);
}
