/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#if defined(__BIONIC__)
#include <android-base/properties.h>
#endif

#include <dlfcn.h>
#include <libgen.h>
#include <limits.h>
#include <link.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/stat.h>

#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <type_traits>

#include "gtest_globals.h"
#include <android-base/file.h>
#include "utils.h"
#ifdef __CHERI_PURE_CAPABILITY__
#include <sys/cheri.h>
#endif // __CHERI_PURE_CAPABILITY__

extern "C" int main_global_default_serial() {
  return 3370318;
}

extern "C" int main_global_protected_serial() {
  return 2716057;
}

// The following functions are defined in DT_NEEDED
// libdl_preempt_test.so library.

// This one calls main_global_default_serial
extern "C" int main_global_default_get_serial();

// This one calls main_global_protected_serial
extern "C" int main_global_protected_get_serial();

// This one calls lib_global_default_serial
extern "C" int lib_global_default_get_serial();

// This one calls lib_global_protected_serial
extern "C" int lib_global_protected_get_serial();

// This test verifies that the global default function
// main_global_default_serial() is preempted by
// the function defined above.
TEST(dl, main_preempts_global_default) {
  ASSERT_EQ(3370318, main_global_default_get_serial());
}

// This one makes sure that the global protected
// symbols do not get preempted
TEST(dl, main_does_not_preempt_global_protected) {
  ASSERT_EQ(3370318, main_global_protected_get_serial());
}

// check same things for lib
TEST(dl, lib_preempts_global_default) {
  ASSERT_EQ(3370318, lib_global_default_get_serial());
}

TEST(dl, lib_does_not_preempt_global_protected) {
  ASSERT_EQ(3370318, lib_global_protected_get_serial());
}

#if defined(__BIONIC__)
#if defined(__LP64__)
#ifdef __CHERI_PURE_CAPABILITY__
  static constexpr const char* kPathToLinker = "/system/bin/linkerc64";
#else /* !__CHERI_PURE_CAPABILITY__ */
  static constexpr const char* kPathToLinker = "/system/bin/linker64";
#endif /* !__CHERI_PURE_CAPABILITY__ */
#else
  static constexpr const char* kPathToLinker = "/system/bin/linker";
#endif

#if defined (__aarch64__)
  static constexpr const char* kAlternatePathToLinker = "/system/bin/arm64/linker64";
#elif defined (__arm__)
  static constexpr const char* kAlternatePathToLinker = "/system/bin/arm/linker";
#elif defined (__x86_64__)
  static constexpr const char* kAlternatePathToLinker = "/system/bin/x86_64/linker64";
#elif defined (__i386__)
  static constexpr const char* kAlternatePathToLinker = "/system/bin/x86/linker";
#else
#error "Unknown architecture"
#endif

const char* PathToLinker() {
  // On the systems with emulated architecture linker would be of different
  // architecture. Try to use alternate paths first.
  struct stat buffer;
  if (stat(kAlternatePathToLinker, &buffer) == 0) {
    return kAlternatePathToLinker;
  }
  return kPathToLinker;
}
#endif  // defined(__BIONIC__)

TEST(dl, exec_linker) {
#if defined(__BIONIC__)
  const char* path_to_linker = PathToLinker();
  std::string usage_prefix = std::string("Usage: ") + path_to_linker;
  ExecTestHelper eth;
  eth.SetArgs({ path_to_linker, nullptr });
  eth.Run([&]() { execve(path_to_linker, eth.GetArgs(), eth.GetEnv()); }, 0, nullptr);
  ASSERT_EQ(0u, eth.GetOutput().find(usage_prefix)) << "Test output:\n" << eth.GetOutput();
#endif
}

TEST(dl, exec_linker_load_file) {
#if defined(__BIONIC__)
  const char* path_to_linker = PathToLinker();
  std::string helper = GetTestlibRoot() +
      "/exec_linker_helper/exec_linker_helper";
  std::string expected_output =
      "ctor: argc=1 argv[0]=" + helper + "\n" +
      "main: argc=1 argv[0]=" + helper + "\n" +
      "__progname=exec_linker_helper\n" +
      "helper_func called\n";
  ExecTestHelper eth;
  eth.SetArgs({ path_to_linker, helper.c_str(), nullptr });
  eth.Run([&]() { execve(path_to_linker, eth.GetArgs(), eth.GetEnv()); }, 0, expected_output.c_str());
#endif
}

TEST(dl, exec_linker_load_from_zip) {
#if defined(__BIONIC__)
  const char* path_to_linker = PathToLinker();
  std::string helper = GetTestlibRoot() +
      "/libdlext_test_zip/libdlext_test_zip_zipaligned.zip!/libdir/exec_linker_helper";
  std::string expected_output =
      "ctor: argc=1 argv[0]=" + helper + "\n" +
      "main: argc=1 argv[0]=" + helper + "\n" +
      "__progname=exec_linker_helper\n" +
      "helper_func called\n";
  ExecTestHelper eth;
  eth.SetArgs({ path_to_linker, helper.c_str(), nullptr });
  eth.Run([&]() { execve(path_to_linker, eth.GetArgs(), eth.GetEnv()); }, 0, expected_output.c_str());
#endif
}

TEST(dl, exec_linker_load_self) {
#if defined(__BIONIC__)
  const char* path_to_linker = PathToLinker();
  std::string error_message = "error: linker cannot load itself\n";
  ExecTestHelper eth;
  eth.SetArgs({ path_to_linker, path_to_linker, nullptr });
  eth.Run([&]() { execve(path_to_linker, eth.GetArgs(), eth.GetEnv()); }, EXIT_FAILURE, error_message.c_str());
#endif
}

TEST(dl, preinit_system_calls) {
#if defined(__BIONIC__)
  SKIP_WITH_HWASAN << "hwasan not initialized in preinit_array, b/124007027";
  std::string helper = GetTestlibRoot() +
      "/preinit_syscall_test_helper/preinit_syscall_test_helper";
  chmod(helper.c_str(), 0755); // TODO: "x" lost in CTS, b/34945607
  ExecTestHelper eth;
  eth.SetArgs({ helper.c_str(), nullptr });
  eth.Run([&]() { execve(helper.c_str(), eth.GetArgs(), eth.GetEnv()); }, 0, nullptr);
#endif
}

TEST(dl, preinit_getauxval) {
#if defined(__BIONIC__)
  SKIP_WITH_HWASAN << "hwasan not initialized in preinit_array, b/124007027";
  std::string helper = GetTestlibRoot() +
      "/preinit_getauxval_test_helper/preinit_getauxval_test_helper";
  chmod(helper.c_str(), 0755); // TODO: "x" lost in CTS, b/34945607
  ExecTestHelper eth;
  eth.SetArgs({ helper.c_str(), nullptr });
  eth.Run([&]() { execve(helper.c_str(), eth.GetArgs(), eth.GetEnv()); }, 0, nullptr);
#else
  // Force a failure when not compiled for bionic so the test is considered a pass.
  ASSERT_TRUE(false);
#endif
}


TEST(dl, exec_without_ld_preload) {
#if defined(__BIONIC__)
  std::string helper = GetTestlibRoot() +
      "/ld_preload_test_helper/ld_preload_test_helper";
  chmod(helper.c_str(), 0755);
  ExecTestHelper eth;
  eth.SetArgs({ helper.c_str(), nullptr });
  eth.Run([&]() { execve(helper.c_str(), eth.GetArgs(), eth.GetEnv()); }, 0, "12345");
#endif
}

TEST(dl, exec_with_ld_preload) {
#if defined(__BIONIC__)
  std::string helper = GetTestlibRoot() +
      "/ld_preload_test_helper/ld_preload_test_helper";
  std::string env = std::string("LD_PRELOAD=") + GetTestlibRoot() + "/ld_preload_test_helper_lib2.so";
  chmod(helper.c_str(), 0755);
  ExecTestHelper eth;
  eth.SetArgs({ helper.c_str(), nullptr });
  eth.SetEnv({ env.c_str(), nullptr });
  // ld_preload_test_helper calls get_value_from_lib() and returns the value.
  // The symbol is defined by two libs: ld_preload_test_helper_lib.so and
  // ld_preloaded_lib.so. The former is DT_NEEDED and the latter is LD_PRELOADED
  // via this execution. The main executable is linked to the LD_PRELOADED lib
  // and the value given from the lib is returned.
  eth.Run([&]() { execve(helper.c_str(), eth.GetArgs(), eth.GetEnv()); }, 0, "54321");
#endif
}


// ld_config_test_helper must fail because it is depending on a lib which is not
// in the search path
//
// Call sequence is...
// _helper -- (get_value_from_lib()) -->
//     _lib1.so -- (get_value_from_another_lib()) -->
//       _lib2.so (returns 12345)
// The two libs are in ns2/ subdir.
TEST(dl, exec_without_ld_config_file) {
#if defined(__BIONIC__)
  std::string error_message =
      "CANNOT LINK EXECUTABLE \"" + GetTestlibRoot() +
      "/ld_config_test_helper/ld_config_test_helper\": library \"ld_config_test_helper_lib1.so\" "
      "not found: needed by main executable\n";
  std::string helper = GetTestlibRoot() +
      "/ld_config_test_helper/ld_config_test_helper";
  chmod(helper.c_str(), 0755);
  ExecTestHelper eth;
  eth.SetArgs({ helper.c_str(), nullptr });
  eth.Run([&]() { execve(helper.c_str(), eth.GetArgs(), eth.GetEnv()); }, EXIT_FAILURE, error_message.c_str());
#endif
}

#if defined(__BIONIC__)
extern "C" void android_get_LD_LIBRARY_PATH(char*, size_t);
static void create_ld_config_file(const char* config_file) {
  char default_search_paths[PATH_MAX];
  android_get_LD_LIBRARY_PATH(default_search_paths, sizeof(default_search_paths));

  std::ofstream fout(config_file, std::ios::out);
  fout << "dir.test = " << GetTestlibRoot() << "/ld_config_test_helper/" << std::endl
       << "[test]" << std::endl
       << "additional.namespaces = ns2" << std::endl
       << "namespace.default.search.paths = " << GetTestlibRoot() << std::endl
       << "namespace.default.links = ns2" << std::endl
       << "namespace.default.link.ns2.shared_libs = libc.so:libm.so:libdl.so:ld_config_test_helper_lib1.so" << std::endl
       << "namespace.ns2.search.paths = " << default_search_paths << ":" << GetTestlibRoot() << "/ns2" << std::endl;
  fout.close();
}
#endif

#if defined(__BIONIC__)
static bool is_debuggable_build() {
  return android::base::GetBoolProperty("ro.debuggable", false);
}
#endif

// _lib1.so and _lib2.so are now searchable by having another namespace 'ns2'
// whose search paths include the 'ns2/' subdir.
TEST(dl, exec_with_ld_config_file) {
#if defined(__BIONIC__)
  SKIP_WITH_HWASAN << "libclang_rt.hwasan is not found with custom ld config";
  if (!is_debuggable_build()) {
    GTEST_SKIP() << "LD_CONFIG_FILE is not supported on user build";
  }
  std::string helper = GetTestlibRoot() +
      "/ld_config_test_helper/ld_config_test_helper";
  TemporaryFile config_file;
  create_ld_config_file(config_file.path);
  std::string env = std::string("LD_CONFIG_FILE=") + config_file.path;
  chmod(helper.c_str(), 0755);
  ExecTestHelper eth;
  eth.SetArgs({ helper.c_str(), nullptr });
  eth.SetEnv({ env.c_str(), nullptr });
  eth.Run([&]() { execve(helper.c_str(), eth.GetArgs(), eth.GetEnv()); }, 0, "12345");
#endif
}

// _lib3.so has same symbol as lib2.so but returns 54321. _lib3.so is
// LD_PRELOADed. This test is to ensure LD_PRELOADed libs are available to
// additional namespaces other than the default namespace.
TEST(dl, exec_with_ld_config_file_with_ld_preload) {
#if defined(__BIONIC__)
  SKIP_WITH_HWASAN << "libclang_rt.hwasan is not found with custom ld config";
  if (!is_debuggable_build()) {
    GTEST_SKIP() << "LD_CONFIG_FILE is not supported on user build";
  }
  std::string helper = GetTestlibRoot() +
      "/ld_config_test_helper/ld_config_test_helper";
  TemporaryFile config_file;
  create_ld_config_file(config_file.path);
  std::string env = std::string("LD_CONFIG_FILE=") + config_file.path;
  std::string env2 = std::string("LD_PRELOAD=") + GetTestlibRoot() + "/ld_config_test_helper_lib3.so";
  chmod(helper.c_str(), 0755);
  ExecTestHelper eth;
  eth.SetArgs({ helper.c_str(), nullptr });
  eth.SetEnv({ env.c_str(), env2.c_str(), nullptr });
  eth.Run([&]() { execve(helper.c_str(), eth.GetArgs(), eth.GetEnv()); }, 0, "54321");
#endif
}

// ensures that LD_CONFIG_FILE env var does not work for production builds.
// The test input is the same as exec_with_ld_config_file, but it must fail in
// this case.
TEST(dl, disable_ld_config_file) {
#if defined(__BIONIC__)
  if (getuid() == 0) {
    // when executed from the shell (e.g. not as part of CTS), skip the test.
    // This test is only for CTS.
    GTEST_SKIP() << "test is not supported with root uid";
  }
  if (is_debuggable_build()) {
    GTEST_SKIP() << "test is not supported on debuggable build";
  }

  std::string error_message = std::string("CANNOT LINK EXECUTABLE ") +
      "\"" + GetTestlibRoot() + "/ld_config_test_helper/ld_config_test_helper\": " +
      "library \"ld_config_test_helper_lib1.so\" not found: needed by main executable\n";
  std::string helper = GetTestlibRoot() +
      "/ld_config_test_helper/ld_config_test_helper";
  TemporaryFile config_file;
  create_ld_config_file(config_file.path);
  std::string env = std::string("LD_CONFIG_FILE=") + config_file.path;
  chmod(helper.c_str(), 0755);
  ExecTestHelper eth;
  eth.SetArgs({ helper.c_str(), nullptr });
  eth.SetEnv({ env.c_str(), nullptr });
  eth.Run([&]() { execve(helper.c_str(), eth.GetArgs(), eth.GetEnv()); }, EXIT_FAILURE, error_message.c_str());
#endif
}

static void RelocationsTest(const char* lib, const char* expectation) {
#if defined(__BIONIC__)
  // Does readelf think the .so file looks right?
  const std::string path = GetTestlibRoot() + "/" + lib;
  ExecTestHelper eth;
  eth.SetArgs({ "readelf", "-SW", path.c_str(), nullptr });
  eth.Run([&]() { execvpe("readelf", eth.GetArgs(), eth.GetEnv()); }, 0, nullptr);
  ASSERT_TRUE(eth.GetOutput().find(expectation) != std::string::npos) << eth.GetOutput();

  // Can we load it?
  void* handle = dlopen(lib, RTLD_NOW);
  ASSERT_TRUE(handle != nullptr) << dlerror();
#else
  UNUSED(lib);
  UNUSED(expectation);
  GTEST_SKIP() << "test is not supported on glibc";
#endif
}

TEST(dl, relocations_RELR) {
#ifdef __CHERI_PURE_CAPABILITY__
  GTEST_UNSUPPORTED_IN_PURE_CAP() << "relr section is not yet emitted in the Pure-cap ABI";
#endif /* __CHERI_PURE_CAPABILITY__ */
  RelocationsTest("librelocations-RELR.so",
      ".relr.dyn            RELR");
}

TEST(dl, relocations_ANDROID_RELR) {
#ifdef __CHERI_PURE_CAPABILITY__
  GTEST_UNSUPPORTED_IN_PURE_CAP() << "relr section is not yet emitted in the Pure-cap ABI";
#endif /* __CHERI_PURE_CAPABILITY__ */
  RelocationsTest("librelocations-ANDROID_RELR.so",
      ".relr.dyn            ANDROID_RELR");
}

TEST(dl, relocations_ANDROID_REL) {
  RelocationsTest("librelocations-ANDROID_REL.so",
#if __LP64__
      ".rela.dyn            ANDROID_RELA"
#else
      ".rel.dyn             ANDROID_REL"
#endif
      );
}

TEST(dl, relocations_fat) {
  RelocationsTest("librelocations-fat.so",
#if __LP64__
      ".rela.dyn            RELA"
#else
      ".rel.dyn             REL"
#endif
      );
}

#ifdef __CHERI_PURE_CAPABILITY__
extern const char* libdl_purecap_relocations_f1();
extern int libdl_purecap_relocations_g1;
extern const int libdl_purecap_relocations_g2;
extern char libdl_purecap_relocations_g3[12];
extern char *libdl_purecap_relocations_g4;
extern const char *libdl_purecap_relocations_g5;

static void verify_symbol_capability(const void *symbol_ptr,
                                     bool expected_writable,
                                     bool expected_executable,
                                     bool expected_sealed,
                                     bool expected_sentry,
                                     ptraddr_t expected_base,
                                     ptraddr_t expected_limit) {
  size_t perms = cheri_perms_get(symbol_ptr);
  bool is_sealed = cheri_is_sealed(symbol_ptr);
  bool is_sentry = cheri_is_sentry(symbol_ptr);
  ptraddr_t base = cheri_base_get(symbol_ptr);
  ptraddr_t limit = base + cheri_length_get(symbol_ptr);

  bool is_writable = (perms & (CHERI_PERM_STORE | CHERI_PERM_STORE_CAP)) != 0;
  bool is_executable = (perms & (CHERI_PERM_EXECUTE)) != 0;

  ASSERT_EQ(is_writable, expected_writable);
  ASSERT_EQ(is_executable, expected_executable);
  ASSERT_EQ(is_sealed, expected_sealed);
  ASSERT_EQ(is_sentry, expected_sentry);
  ASSERT_EQ(base, expected_base);
  ASSERT_EQ(limit, expected_limit);
  ASSERT_EQ((int)(perms & CHERI_PERM_SW_VMEM), 0);
}

template<typename T>
static void verify_data_symbol_capability(T* symbol_ptr) {
  bool expected_writable = !std::is_const<T>::value;
  verify_symbol_capability(symbol_ptr,
                           expected_writable,
                           false,
                           false,
                           false,
                           cheri_address_get(symbol_ptr),
                           cheri_address_get(symbol_ptr) + sizeof(T));
}

static void get_expected_bounds_for_executable_symbol(const void *symbol_ptr,
                                                      ptraddr_t *out_base,
                                                      ptraddr_t *out_limit) {
  struct CallBackParam {
    dl_phdr_info phdr_info;
    ptraddr_t addr;
  } callback_param = { dl_phdr_info{}, cheri_address_get(symbol_ptr) };

  int res = dl_iterate_phdr([](dl_phdr_info* info, size_t, void* param) {
                              ptraddr_t addr = reinterpret_cast<CallBackParam*>(param)->addr;
                              if (cheri_base_get(info->dlpi_addr) <= addr
                                  && addr < cheri_base_get(info->dlpi_addr)
                                            + cheri_length_get(info->dlpi_addr)) {
                                reinterpret_cast<CallBackParam*>(param)->phdr_info = *info;
                                return 1;
                              }
                              return 0;
                            },
                            &callback_param);
  ASSERT_EQ(res, 1);

  Elf64_Addr min_rx_addr = (Elf64_Addr) -1, max_rx_addr = 0;
  const dl_phdr_info& phdr_info = callback_param.phdr_info;
  for (size_t phdr_index = 0; phdr_index < phdr_info.dlpi_phnum; phdr_index++) {
    if ((phdr_info.dlpi_phdr[phdr_index].p_type == PT_LOAD
        && (phdr_info.dlpi_phdr[phdr_index].p_flags & PF_W) == 0)
        || phdr_info.dlpi_phdr[phdr_index].p_type == PT_GNU_RELRO) {
      min_rx_addr = std::min(min_rx_addr,
                             phdr_info.dlpi_phdr[phdr_index].p_vaddr);
      max_rx_addr = std::max(max_rx_addr,
                             phdr_info.dlpi_phdr[phdr_index].p_vaddr
                             + phdr_info.dlpi_phdr[phdr_index].p_memsz);
    }
  }

  *out_base = phdr_info.dlpi_addr + min_rx_addr;
  *out_limit = phdr_info.dlpi_addr + max_rx_addr;
}

TEST(dl, purecap_relocations) {
  ptraddr_t expected_f1_base, expected_f1_limit;
  const void* f1_ptr = reinterpret_cast<void*>(&libdl_purecap_relocations_f1);
  get_expected_bounds_for_executable_symbol(f1_ptr,
                                            &expected_f1_base,
                                            &expected_f1_limit);

  verify_symbol_capability(f1_ptr,
                           false, true, true, true,
                           expected_f1_base, expected_f1_limit);

  verify_data_symbol_capability(&libdl_purecap_relocations_g1);
  verify_data_symbol_capability(&libdl_purecap_relocations_g2);
  verify_data_symbol_capability(&libdl_purecap_relocations_g3);
  verify_data_symbol_capability(&libdl_purecap_relocations_g4);
  verify_data_symbol_capability(&libdl_purecap_relocations_g5);

  ASSERT_TRUE(!strcmp("f1", libdl_purecap_relocations_f1()));

  ASSERT_EQ(libdl_purecap_relocations_g1, 123);
  ASSERT_EQ(libdl_purecap_relocations_g2, 124);
  ASSERT_EQ(libdl_purecap_relocations_g4, libdl_purecap_relocations_g3 + 5);
  ASSERT_EQ(libdl_purecap_relocations_g5, libdl_purecap_relocations_g3 + 10);

  ASSERT_EQ(cheri_base_get(libdl_purecap_relocations_g4), cheri_base_get(&libdl_purecap_relocations_g3[0]));
  ASSERT_EQ(cheri_base_get(libdl_purecap_relocations_g5), cheri_base_get(&libdl_purecap_relocations_g3[0]));

  ASSERT_EQ(cheri_length_get(libdl_purecap_relocations_g4), cheri_length_get(&libdl_purecap_relocations_g3[0]));
  ASSERT_EQ(cheri_length_get(libdl_purecap_relocations_g5), cheri_length_get(&libdl_purecap_relocations_g3[0]));
}

TEST(dl, purecap_no_dso_bounds_intersection) {
  struct base_and_limit {
    ptraddr_t base;
    ptraddr_t limit;
  };
  typedef std::vector<base_and_limit> bounds_vec_t;

  bounds_vec_t dso_bounds;

  int res = dl_iterate_phdr([](dl_phdr_info* info, size_t, void* param) {
                              bounds_vec_t *output = reinterpret_cast<bounds_vec_t*>(param);
                              output->push_back(
                                  base_and_limit
                                  {
                                    cheri_base_get(info->dlpi_addr),
                                    cheri_base_get(info->dlpi_addr) + cheri_length_get(info->dlpi_addr)
                                  });

                              return 0;
                            },
                            &dso_bounds);
  ASSERT_EQ(res, 0);

  for (size_t i = 0; i < dso_bounds.size(); i++) {
    for (size_t j = i + 1; j < dso_bounds.size(); j++) {
      ASSERT_FALSE(dso_bounds[i].base >= dso_bounds[j].base && dso_bounds[i].base < dso_bounds[j].limit);
      ASSERT_FALSE(dso_bounds[j].base >= dso_bounds[i].base && dso_bounds[j].base < dso_bounds[i].limit);
    }
  }
}
#endif /* __CHERI_PURE_CAPABILITY__ */
