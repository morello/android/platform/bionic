/*
 * Copyright (C) 2013, 2022 The Android Open Source Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <private/bionic_asm.h>

ENTRY(syscall)
    /* Move syscall No. from x0 to x8 */
    mov     x8, x0

    gclen   x1, c9
    add     c9, c9, x1
    lsr     x1, x1, #2
    cmp     x1, #(4 * 6)
    csneg   x1, xzr, x1, hi
    adr     c0, .Lpost_load_varargs+1
    add     c0, c0, x1

    mov     x1, xzr
    mov     x2, xzr
    mov     x3, xzr
    mov     x4, xzr
    mov     x5, xzr
    mov     x6, xzr

    br      c0

.Lload_varargs:
    ldr     c5, [c9, #-16]!  /* gclen(c9) <= 6 * 16 */
    ldr     c4, [c9, #-16]!  /* gclen(c9) <= 5 * 16 */
    ldr     c3, [c9, #-16]!  /* gclen(c9) <= 4 * 16 */
    ldr     c2, [c9, #-16]!  /* gclen(c9) <= 3 * 16 */
    ldr     c1, [c9, #-16]!  /* gclen(c9) <= 2 * 16 */
    ldr     c0, [c9, #-16]!  /* gclen(c9) <= 1 * 16 */
.Lpost_load_varargs:         /* gclen(c9) == 0 or gclen(c9) > 6 * 16 */

#ifndef USE_LIBSHIM
    svc     #0
#else /* USE_LIBSHIM */
    str     clr, [csp, #-16]!
    /* Move syscall parameters from c0 thru c5 to c1 thru c6 */
    mov     c6, c5
    mov     c5, c4
    mov     c4, c3
    mov     c3, c2
    mov     c2, c1
    mov     c1, c0
    mov     x0, x8
    bl      __shim_syscall
    ldr     clr, [csp], #16
#endif /* USE_LIBSHIM */

    /* check if syscall returned successfully */
    cmn     x0, #(MAX_ERRNO + 1)
    b.ls    1f
    neg     x0, x0
    b       __set_errno_internal

1:
    ret
END(syscall)
