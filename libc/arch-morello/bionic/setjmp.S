/*
 * Copyright (C) 2013 The Android Open Source Project
 * Copyright (C) 2020 Arm Limited
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <private/bionic_asm.h>
#include <private/bionic_constants.h>

// According to AARCH64 PCS document we need to save the following
// registers:
//
// Core     c19 - c30, csp
// VFP      d8 - d15
//
// NOTE: AAPCS mandates that the higher part of q registers do not need to
//       be saved by the callee.
//
// The internal structure of a jmp_buf is totally private.
// Current layout (changes from release to release):
//
// 64-bit word   name            description
// 0             sigflag/cookie  setjmp cookie in top 31 bits, signal mask flag in low bit
// 1             sigmask         signal mask (not used with _setjmp / _longjmp)
// 2             core_base       base of core registers (c19-c30, csp)
// 28            float_base      base of float registers (d8-d15)
// 36            checksum        checksum of core registers
// 37            reserved

#define _JB_SIGFLAG     0
#define _JB_SIGMASK     (_JB_SIGFLAG + 1)
#define _JB_C30_CSP     (_JB_SIGMASK + 1)
#define _JB_C28_C29     (_JB_C30_CSP + 4)
#define _JB_C26_C27     (_JB_C28_C29 + 4)
#define _JB_C24_C25     (_JB_C26_C27 + 4)
#define _JB_C22_C23     (_JB_C24_C25 + 4)
#define _JB_C20_C21     (_JB_C22_C23 + 4)
#define _JB_C19         (_JB_C20_C21 + 4)
#define _JB_D14_D15     (_JB_C19     + 2)
#define _JB_D12_D13     (_JB_D14_D15 + 2)
#define _JB_D10_D11     (_JB_D12_D13 + 2)
#define _JB_D8_D9       (_JB_D10_D11 + 2)
#define _JB_CHECKSUM    (_JB_D8_D9 + 2)

#define USE_CHECKSUM 1

#if __has_feature(hwaddress_sanitizer)
#error "hwaddress_sanitizer isn't supported for Morello"
#endif

.macro m_calculate_checksum dst, src, tmp1, tmp2
  mov \dst, #0
  .irp i,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
    ldp \tmp1, \tmp2, [\src, #(\i * 16)]
    eor \dst, \dst, \tmp1
    eor \dst, \dst, \tmp2
  .endr
.endm

ENTRY(setjmp)
__BIONIC_WEAK_ASM_FOR_NATIVE_BRIDGE(setjmp)
  mov w1, #1
  b sigsetjmp
END(setjmp)

ENTRY(_setjmp)
__BIONIC_WEAK_ASM_FOR_NATIVE_BRIDGE(_setjmp)
  mov w1, #0
  b sigsetjmp
END(_setjmp)

// int sigsetjmp(sigjmp_buf env, int save_signal_mask);
ENTRY(sigsetjmp)
__BIONIC_WEAK_ASM_FOR_NATIVE_BRIDGE(sigsetjmp)
  stp c0, c30, [csp, #-32]!
  .cfi_def_cfa_offset 32
  .cfi_rel_offset c0, 0
  .cfi_rel_offset c30, 16

  // Get the cookie and store it along with the signal flag.
  mov c0, c1
  bl __bionic_setjmp_cookie_get
  mov c1, c0
  ldr c0, [csp, #0]
  str x1, [c0, #(_JB_SIGFLAG * 8)]

  // Do we need to save the signal mask?
  tbz w1, #0, 1f

  // Save the cookie for later.
  str c1, [csp, #-16]!
  .cfi_adjust_cfa_offset 16

  // Save current signal mask.
  // The 'how' argument is ignored if new_mask is NULL.
  mov x1, #0 // NULL.
  add c2, c0, #(_JB_SIGMASK * 8) // old_mask.
  bl sigprocmask

  ldr c1, [csp], #16
  .cfi_adjust_cfa_offset -16

1:
  // Restore original x0 and lr.
  ldp c0, c30, [csp], #32
  .cfi_adjust_cfa_offset -32
  .cfi_restore c0
  .cfi_restore c30

  // Mask off the signal flag bit.
  bic x1, x1, #1

  // Save core registers.
  mov c7, csp
  stp c30, c7,  [c0, #(_JB_C30_CSP * 8)]
  stp c28, c29, [c0, #(_JB_C28_C29 * 8)]
  stp c26, c27, [c0, #(_JB_C26_C27 * 8)]
  stp c24, c25, [c0, #(_JB_C24_C25 * 8)]
#if !defined(MORELLO_16REGS_ONLY)
  stp c22, c23, [c0, #(_JB_C22_C23 * 8)]
  stp c20, c21, [c0, #(_JB_C20_C21 * 8)]
  str c19,      [c0, #(_JB_C19     * 8)]
#endif

  // Save floating point registers.
  stp d14, d15, [c0, #(_JB_D14_D15 * 8)]
  stp d12, d13, [c0, #(_JB_D12_D13 * 8)]
  stp d10, d11, [c0, #(_JB_D10_D11 * 8)]
  stp d8,  d9,  [c0, #(_JB_D8_D9   * 8)]

#if USE_CHECKSUM
  // Calculate the checksum.
  str xzr, [c0, #(_JB_CHECKSUM * 8)]
  m_calculate_checksum x12, c0, x2, x3
  str x12, [c0, #(_JB_CHECKSUM * 8)]
#endif

  mov w0, #0
  ret
END(sigsetjmp)

// void siglongjmp(sigjmp_buf env, int value);
ENTRY(siglongjmp)
__BIONIC_WEAK_ASM_FOR_NATIVE_BRIDGE(siglongjmp)
#if USE_CHECKSUM
  // Check the checksum before doing anything.
  m_calculate_checksum x12, c0, x2, x3
  cbnz    x12, __bionic_setjmp_checksum_mismatch
#endif

  // Do we need to restore the signal mask?
  ldr x2, [c0, #(_JB_SIGFLAG * 8)]
  tbz w2, #0, 1f

  stp c0, c30, [csp, #-32]!
  .cfi_adjust_cfa_offset 32
  .cfi_rel_offset c0, 0
  .cfi_rel_offset c30, 16

  // Restore signal mask.
  mov c25, c1 // Save 'value'.

  mov c2, c0
  mov x0, #2 // SIG_SETMASK
  add c1, c2, #(_JB_SIGMASK * 8) // new_mask.
  mov x2, #0 // NULL.
  bl sigprocmask
  mov c1, c25 // Restore 'value'.

  // Restore original x0 and lr.
  ldp c0, c30, [csp], #32
  .cfi_adjust_cfa_offset -32
  .cfi_restore c0
  .cfi_restore c30

  ldr x2, [c0, #(_JB_SIGFLAG * 8)]
1:
  // Restore core registers.
  bic x2, x2, #1
  ldp c30, c7,  [c0, #(_JB_C30_CSP * 8)]
  ldp c28, c29, [c0, #(_JB_C28_C29 * 8)]
  ldp c26, c27, [c0, #(_JB_C26_C27 * 8)]
  ldp c24, c25, [c0, #(_JB_C24_C25 * 8)]
#if !defined(MORELLO_16REGS_ONLY)
  ldp c22, c23, [c0, #(_JB_C22_C23 * 8)]
  ldp c20, c21, [c0, #(_JB_C20_C21 * 8)]
  ldr c19,      [c0, #(_JB_C19     * 8)]
#endif
  mov csp, c7

  stp c0, c1, [csp, #-32]!
  .cfi_adjust_cfa_offset 32
  .cfi_rel_offset c0, 0
  .cfi_rel_offset c1, 16
  str c30, [csp, #-16]!
  .cfi_adjust_cfa_offset 16
  .cfi_rel_offset c30, 0
  ldr x0, [c0, #(_JB_SIGFLAG * 8)]
  bl __bionic_setjmp_cookie_check
  ldr c30, [csp], #16
  .cfi_adjust_cfa_offset -16
  .cfi_restore c30
  ldp c0, c1, [csp], #32
  .cfi_adjust_cfa_offset -32
  .cfi_restore c0
  .cfi_restore c1

  // Restore floating point registers.
  ldp d14, d15, [c0, #(_JB_D14_D15 * 8)]
  ldp d12, d13, [c0, #(_JB_D12_D13 * 8)]
  ldp d10, d11, [c0, #(_JB_D10_D11 * 8)]
  ldp d8,  d9,  [c0, #(_JB_D8_D9   * 8)]

  // Set return value.
  cmp w1, wzr
  csinc w0, w1, wzr, ne
  ret
END(siglongjmp)

ALIAS_SYMBOL(longjmp, siglongjmp)
__BIONIC_WEAK_ASM_FOR_NATIVE_BRIDGE(longjmp)
ALIAS_SYMBOL(_longjmp, siglongjmp)
__BIONIC_WEAK_ASM_FOR_NATIVE_BRIDGE(_longjmp)
