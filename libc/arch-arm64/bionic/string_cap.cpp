/*
 * Copyright (C) 2017-2020 ARM Ltd
 * Copyright (C) 2017 The Android Open Source Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <string.h>
#include <stdint.h>
#include <wchar.h>

#ifdef __CHERI__
#include <cheriintrin.h>
#endif

#define NO_BUILTIN __attribute__((no_builtin))

/* How many elements are copied each iteration of the 4X unrolled loop.  */
constexpr size_t kBlockSize = 4;

template <typename T>
struct add_pointer {
  using type = T*;
};

#ifdef __CHERI__
typedef uintcap_t mem_granule_t;

template <typename T>
struct add_capability {
  using type = T* __capability;
};

template <typename Element>
static size_t GetAlignmentOffset(const void* __capability p) {
  return cheri_address_get(p) & (sizeof(Element) - 1);
}
#else /* !__CHERI__ */
typedef uintptr_t mem_granule_t;
#endif /* !__CHERI__ */

#ifndef __CHERI_PURE_CAPABILITY__
template <typename Element>
static size_t GetAlignmentOffset(const void* p) {
  return static_cast<ptraddr_t>(reinterpret_cast<uintptr_t>(p)) & (sizeof(Element) - 1);
}
#endif /* !__CHERI_PURE_CAPABILITY__ */

// ForwardCopier and BackwardCopier implement simple copy algorithms, either element by element, or
// block by block.
// src and dst are the start of the memory to copy for ForwardCopier, and the end (excluded) for
// ForwardCopier. They are passed by reference as they need to be updated in the caller (since
// everything is inlined, this is effectively free).
// All functions work for both pointers and capabilities (we need both T and U as template
// arguments, because U is a pointer/capability to const, unlike U).
struct ForwardCopier {
  template <typename T, typename U>
  static size_t Copy(T& dst, U& src, size_t len) NO_BUILTIN {
    while (len >= sizeof(*src)) {
      *dst++ = *src++;
      len -= sizeof(*src);
    }

    return len;
  }

  template <typename T, typename U>
  static size_t CopyBlock(T& dst, U& src, size_t len) NO_BUILTIN {
    while (len >= kBlockSize * sizeof(*src)) {
      *dst++ = *src++;
      *dst++ = *src++;
      *dst++ = *src++;
      *dst++ = *src++;
      len -= kBlockSize * sizeof(*src);
    }

    return len;
  }

  template <typename Element, typename T, typename U> NO_BUILTIN
  static size_t CopyUntilAligned(T& dst, U& src) {
    static_assert(sizeof(*src) == sizeof(char), "");

    // Copy until the next element.
    // The masking is necessary in case the alignment is 0 (in which case nothing should be copied).
    size_t offset = (sizeof(Element) - GetAlignmentOffset<Element>(src)) & (sizeof(Element) - 1);
    Copy(dst, src, offset);
    return offset;
  }
};

struct BackwardCopier {
  template <typename T, typename U> NO_BUILTIN
  static size_t Copy(T& dst, U& src, size_t len) {
    while (len >= sizeof(*src)) {
      *--dst = *--src;
      len -= sizeof(*src);
    }

    return len;
  }

  template <typename T, typename U> NO_BUILTIN
  static size_t CopyBlock(T& dst, U& src, size_t len) {
    while (len >= kBlockSize * sizeof(*src)) {
      *--dst = *--src;
      *--dst = *--src;
      *--dst = *--src;
      *--dst = *--src;
      len -= kBlockSize * sizeof(*src);
    }

    return len;
  }

  template <typename Element, typename T, typename U> NO_BUILTIN
  static size_t CopyUntilAligned(T& dst, U& src) {
    static_assert(sizeof(*src) == sizeof(char), "");

    // Copy (backwards) until the start of this element.
    size_t offset = GetAlignmentOffset<Element>(src);
    Copy(dst, src, offset);
    return offset;
  }
};

template <template <typename> class Ptr, typename Copier> NO_BUILTIN
static typename Ptr<void>::type DoMemcpy(typename Ptr<void>::type dst0,
                                         typename Ptr<const void>::type src0,
                                         size_t len0) {
  auto dst = reinterpret_cast<typename Ptr<char>::type>(dst0);
  auto src = reinterpret_cast<typename Ptr<const char>::type>(src0);

  /* use byte copy if the block is smaller than a granule */
  if (len0 < sizeof(mem_granule_t)) {
    Copier::Copy(dst, src, len0);

    return dst0;
  }

  size_t dst_align = GetAlignmentOffset<mem_granule_t>(dst0);
  size_t src_align = GetAlignmentOffset<mem_granule_t>(src0);

  /* if the two pointers are aligned with respect to each other, then use
   * capabilities where possible
   */
  if (src_align == dst_align) {
    len0 -= Copier::template CopyUntilAligned<mem_granule_t>(dst, src);

    auto aligned_dst = reinterpret_cast<typename Ptr<mem_granule_t>::type>(dst);
    auto aligned_src = reinterpret_cast<typename Ptr<const mem_granule_t>::type>(src);

    /* src and dst are now aligned to a capability. if the size of the remaining
     * block is larger than 4 capabilities, unroll */
    len0 = Copier::CopyBlock(aligned_dst, aligned_src, len0);

    /* copy any remaining capability sized pieces */
    len0 = Copier::Copy(aligned_dst, aligned_src, len0);

    /* copy any remaining bytes */
    dst = reinterpret_cast<typename Ptr<char>::type>(aligned_dst);
    src = reinterpret_cast<typename Ptr<const char>::type>(aligned_src);
    Copier::Copy(dst, src, len0);

    return dst0;
  }

  // else fall back to the original capability unware implementation

  /* If the size is small, or either SRC or DST is unaligned,
     then punt into the byte copy loop.  This should be rare.  */
  if (len0 >= kBlockSize * sizeof(long) &&
      GetAlignmentOffset<long>(src) == 0 && GetAlignmentOffset<long>(src) == 0) {
    auto aligned_dst = reinterpret_cast<typename Ptr<long>::type>(dst);
    auto aligned_src = reinterpret_cast<typename Ptr<const long>::type>(src);

    /* Copy 4X long words at a time if possible.  */
    len0 = Copier::CopyBlock(aligned_dst, aligned_src, len0);

    /* Copy one long word at a time if possible.  */
    len0 = Copier::Copy(aligned_dst, aligned_src, len0);

    /* Pick up any residual with a byte copier.  */
    dst = reinterpret_cast<typename Ptr<char>::type>(aligned_dst);
    src = reinterpret_cast<typename Ptr<const char>::type>(aligned_src);
  }

  Copier::Copy(dst, src, len0);

  return dst0;
}

template <template <typename> class Ptr> NO_BUILTIN
static typename Ptr<void>::type DoMemmove(typename Ptr<void>::type dst0,
                                          typename Ptr<const void>::type src0,
                                          size_t len0) {
  auto dst = reinterpret_cast<typename Ptr<char>::type>(dst0);
  auto src = reinterpret_cast<typename Ptr<const char>::type>(src0);

  if (src < dst && dst < src + len0) {
    // destructive overlap, copy backwards
    DoMemcpy<Ptr, BackwardCopier>(dst + len0, src + len0, len0);
  } else {
    DoMemcpy<Ptr, ForwardCopier>(dst, src, len0);
  }

  return dst0;
}

template <template <typename> class Ptr> NO_BUILTIN
static typename Ptr<void>::type DoMemset(typename Ptr<void>::type dst0,
                                         int value0,
                                         size_t len0) {
  auto dst = reinterpret_cast<typename Ptr<char>::type>(dst0);

  for (size_t offset = 0; offset < len0; offset++) {
    dst[offset] = static_cast<char>(value0);
  }

  return dst0;
}

template <template <typename> class Ptr>
static int DoMemcmp(typename Ptr<const void>::type cs0,
                    typename Ptr<const void>::type ct0,
                    size_t count0) {
  auto cs = reinterpret_cast<typename Ptr<const unsigned char>::type>(cs0);
  auto ct = reinterpret_cast<typename Ptr<const unsigned char>::type>(ct0);
  auto end = cs + count0;
  int res = 0;

  while (cs < end) {
    res = *cs++ - *ct++;
    if (res)
      break;
  }
  return res;
}

#ifndef __CHERI__
void* NO_BUILTIN
memmove(void* dst0,
        const void* src0,
        size_t len0) {
  return DoMemmove<add_pointer>(dst0, src0, len0);
}
#endif

#ifdef __CHERI_PURE_CAPABILITY__
int NO_BUILTIN
memcmp(const void * cs,
       const void * ct,
       size_t count) {
  return DoMemcmp<add_pointer>(cs, ct, count);
}
#endif

wchar_t* NO_BUILTIN
wmemmove(wchar_t* dst0,
         const wchar_t* src0,
         size_t len0) {
  return reinterpret_cast<wchar_t*>(memmove(dst0, src0, sizeof(wchar_t) * len0));
}

#ifndef __CHERI__
void* NO_BUILTIN
memcpy(void* __restrict__ dst0,
       const void* __restrict__ src0,
       size_t len0) {
  return DoMemcpy<add_pointer, ForwardCopier>(dst0, src0, len0);
}
#endif

void* NO_BUILTIN
mempcpy(void* __restrict__ dst0,
        const void* __restrict__ src0,
        size_t len0) {
  return reinterpret_cast<char*>(memcpy(dst0, src0, len0)) + len0;
}

#ifndef __CHERI__
void* NO_BUILTIN
memset(void* dst0,
       int value0,
       size_t len0) {
  return DoMemset<add_pointer>(dst0, value0, len0);
}
#endif

#ifdef __CHERI__
void* __capability NO_BUILTIN
memmove_c(void* __capability dst0,
          const void* __capability src0,
          size_t len0) {
  return DoMemmove<add_capability>(dst0, src0, len0);
}

wchar_t* __capability NO_BUILTIN
wmemmove_c(wchar_t* __capability dst0,
           const wchar_t* __capability src0,
           size_t len0) {
  return reinterpret_cast<wchar_t* __capability>(memmove_c(dst0,
                                                           src0,
                                                           sizeof(wchar_t) * len0));
}

int NO_BUILTIN
memcmp_c(const void * __capability cs,
         const void * __capability ct,
         size_t count) {
  return DoMemcmp<add_capability>(cs, ct, count);
}

void* __capability NO_BUILTIN
memcpy_c(void* __capability __restrict__ dst0,
         const void* __capability __restrict__ src0,
         size_t len0) {
  return DoMemcpy<add_capability, ForwardCopier>(dst0, src0, len0);
}

void* __capability NO_BUILTIN
mempcpy_c(void* __capability __restrict__ dst0,
          const void* __capability __restrict__ src0,
          size_t len0) {
  return reinterpret_cast<char* __capability>(memcpy_c(dst0, src0, len0)) + len0;
}

void* __capability NO_BUILTIN
memset_c(void* __capability dst0,
         int value0,
         size_t len0) {
  return DoMemset<add_capability>(dst0, value0, len0);
}
#endif /* __CHERI__ */
