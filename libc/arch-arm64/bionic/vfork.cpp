/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <unistd.h>

// Replace vfork with a wrapper for fork. This is required as vfork is
// incompatible with libshim.
int vfork() {
  return fork();
}
