/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <errno.h>
#include <string.h>
#include <sys/cdefs.h>
#include <sys/mman.h>
#include <sys/user.h>
#ifdef __CHERI_PURE_CAPABILITY__
#include <cheriintrin.h>
#include <sys/cheri.h>
#endif /* __CHERI_PURE_CAPABILITY__ */

#include <async_safe/log.h>

#include "platform/bionic/macros.h"

// Write protected wrapper class that aligns its contents to a page boundary,
// and sets the memory protection to be non-writable, except when being modified
// explicitly.
template <typename T>
class WriteProtected {
  static_assert(sizeof(T) < PAGE_SIZE,
                "WriteProtected only supports contents up to PAGE_SIZE");
  static_assert(__is_pod(T), "WriteProtected only supports POD contents");

  void *mapping {nullptr};
  T *data {nullptr};

  int set_protection(int prot) {
    auto addr = mapping;
#if __has_feature(hwaddress_sanitizer)
    // The mprotect system call does not currently untag pointers, so do it
    // ourselves.
    addr = untag_address(addr);
#endif
    return mprotect(addr, PAGE_SIZE, prot);
  }

 public:
  WriteProtected() = default;
  ~WriteProtected() {
    if (mapping != nullptr) {
      munmap(mapping, PAGE_SIZE);
    }
  }
  BIONIC_DISALLOW_COPY_AND_ASSIGN(WriteProtected);

  void initialize() {
    mapping = mmap(reinterpret_cast<void*>(0), PAGE_SIZE,
                   PROT_READ | PROT_MAX(PROT_READ | PROT_WRITE),
                   MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    if (mapping == MAP_FAILED) {
      async_safe_fatal("failed to allocate WriteProtected area in initialize");
    } else {
      data = reinterpret_cast<T*>(mapping);
#ifdef __CHERI_PURE_CAPABILITY__
      data = cheri_perms_clear(data, CHERI_PERM_SW_VMEM);
#endif
    }
  }

  bool is_initialized() {
    return data != nullptr;
  }

  const T* operator->() {
    return data;
  }

  const T& operator*() {
    return *data;
  }

  template <typename Mutator>
  void mutate(Mutator mutator) {
    if (set_protection(PROT_READ | PROT_WRITE) != 0) {
      async_safe_fatal("failed to make WriteProtected writable in mutate: %s",
                       strerror(errno));
    }
    mutator(data);
    if (set_protection(PROT_READ) != 0) {
      async_safe_fatal("failed to make WriteProtected nonwritable in mutate: %s",
                       strerror(errno));
    }
  }
};
