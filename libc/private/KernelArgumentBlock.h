/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <elf.h>
#include <link.h>
#include <stdint.h>
#include <sys/auxv.h>

#include "platform/bionic/macros.h"
#include "private/bionic_auxv.h"

// When the kernel starts the dynamic linker, it passes a pointer to a block
// of memory containing argc, the argv array, the environment variable array,
// and the array of ELF aux vectors. This class breaks that block up into its
// constituents for easy access.
class KernelArgumentBlock {
 public:
#ifdef __CHERI_PURE_CAPABILITY__
  explicit KernelArgumentBlock(int argc,
                               char **argv,
                               char **envp,
                               ElfW(auxv_t)* auxv,
                               const void* stack_top)
    : argc(argc),
      argv(argv),
      envp(envp),
      auxv(auxv),
      stack_top(stack_top) {}
#else /* !__CHERI_PURE_CAPABILITY__ */
  explicit KernelArgumentBlock(void* raw_args)
    : stack_top(raw_args) {
    uintptr_t* args = reinterpret_cast<uintptr_t*>(raw_args);
    argc = static_cast<int>(*args);
    argv = reinterpret_cast<char**>(args + 1);
    envp = argv + argc + 1;

    // Skip over all environment variable definitions to find the aux vector.
    // The end of the environment block is marked by a NULL pointer.
    char** p = envp;
    while (*p != nullptr) {
      ++p;
    }
    ++p; // Skip the NULL itself.

    auxv = reinterpret_cast<ElfW(auxv_t)*>(p);
  }
#endif /* !__CHERI_PURE_CAPABILITY__ */

  // Similar to ::getauxval but doesn't require the libc global variables to be set up,
  // so it's safe to call this really early on.
  unsigned long getauxval(unsigned long type) const {
    if (__bionic_is_auxv_pointer(type))
      return 0;

    return getaux_val_or_ptr<unsigned long>([](ElfW(auxv_t)* v) -> unsigned long {
                                            return v->a_un.a_val;
                                            },
                                            type);
  }

  // Similar to ::getauxptr but doesn't require the libc global variables to be set up,
  // so it's safe to call this really early on.
  void* getauxptr(unsigned long type) const {
    if (!__bionic_is_auxv_pointer(type))
      return 0;

    return getaux_val_or_ptr<void*>([](ElfW(auxv_t)* v) -> void* {
                                    return v->a_un.a_ptr;
                                    },
                                    type);
  }

  int argc;
  char** argv;
  char** envp;
  ElfW(auxv_t)* auxv;
  const void* stack_top;

 private:
  BIONIC_DISALLOW_COPY_AND_ASSIGN(KernelArgumentBlock);

  template<typename T>
  __always_inline T getaux_val_or_ptr(T (*field_get)(ElfW(auxv_t)*), unsigned long type) const {
    for (ElfW(auxv_t)* v = auxv; v->a_type != AT_NULL; ++v) {
      if (v->a_type == type) {
        return field_get(v);
      }
    }
    return 0;
  }
};
