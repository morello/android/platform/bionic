/*
 * Copyright (C) 2013 The Android Open Source Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#pragma once

#include <sys/cdefs.h>
#include <sys/auxv.h>

__LIBC_HIDDEN__ unsigned long __bionic_getauxval(unsigned long type, bool& exists);
__LIBC_HIDDEN__ void* __bionic_getauxptr(unsigned long type, bool& exists);

static inline bool __always_inline
__bionic_is_auxv_pointer(unsigned long type) {
  return (type == AT_CHERI_EXEC_RW_CAP
          || type == AT_CHERI_EXEC_RX_CAP
          || type == AT_CHERI_INTERP_RW_CAP
          || type == AT_CHERI_INTERP_RX_CAP
          || type == AT_CHERI_STACK_CAP
          || type == AT_CHERI_SEAL_CAP
          || type == AT_ENTRY
          || type == AT_PHDR
          || type == AT_RANDOM
          || type == AT_PLATFORM
          || type == AT_EXECFN
          || type == AT_ARGV
          || type == AT_ENVP
#if defined(__i386__)
          || type == AT_SYSINFO
#endif /* __i386__ */
          || type == AT_SYSINFO_EHDR);
}
