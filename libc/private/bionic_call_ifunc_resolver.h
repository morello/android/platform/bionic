/*
 * Copyright (C) 2019 The Android Open Source Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#pragma once

#include <link.h>
#include <sys/cdefs.h>
#include <sys/ifunc.h>

#include "private/KernelArgumentBlock.h"

// MORELLO-META: ptr-not-long
//
// An ifunc resolver returns a pointer to a function.
// It is generally true for 32 and 64-bit ELF files that sizeof(ElfW(Addr)) == sizeof(void*),
// however, in architectures supporting capabilities this assumption is false.
// Introduce a generic type, ifunc_t, to emphasise that an ifunc_resolver_t returns a pointer
// to a function and not an integer value which can not be transparently treated as a pointer.
typedef void (*ifunc_t)(void);
#if defined(__aarch64__)
typedef ifunc_t (*ifunc_resolver_t)(uint64_t, __ifunc_arg_t*);
#elif defined(__arm__)
typedef ifunc_t (*ifunc_resolver_t)(unsigned long);
#else
typedef ifunc_t (*ifunc_resolver_t)(void);
#endif

__LIBC_HIDDEN__ ifunc_t __bionic_call_ifunc_resolver(ifunc_resolver_t resolver,
                                                     const KernelArgumentBlock* args);
