/*
 * Copyright (C) 2013 The Android Open Source Project
 * Copyright (C) 2021 Arm Limited
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _MORELLO_ELF_MACHDEP_H_
#define _MORELLO_ELF_MACHDEP_H_

#include <stdint.h>

/* PC-relative addresses */
#define R_MORELLO_LD_PREL_LO17          57348
#define R_MORELLO_ADR_PREL_PG_HI20      57349
#define R_MORELLO_ADR_PREL_PG_HI20_NC   57350

/* Control-flow relocations */
#define R_MORELLO_TSTBR14               57344
#define R_MORELLO_CONDBR19              57345
#define R_MORELLO_JUMP26                57346
#define R_MORELLO_CALL26                57347

#define R_MORELLO_ADR_GOT_PAGE          57351
#define R_MORELLO_LD128_GOT_LO12_NC     57352

/* Dynamic relocations */
#define R_MORELLO_CAPINIT               59392
#define R_MORELLO_GLOB_DAT              59393    /* Create GOT entry.  */
#define R_MORELLO_JUMP_SLOT             59394    /* Create PLT entry.  */
#define R_MORELLO_RELATIVE              59395    /* Adjust by program base.  */
#define R_MORELLO_IRELATIVE             59396
#define R_MORELLO_TLSDESC               59397
#define R_MORELLO_TLS_TPREL128          59398

// The constants of the permissions encoded in the relocation fragment as
// specified by the document:
// Morello Supplement to ELF for the Arm 64-bit Architecture (AArch64)
// version: 2020Q4
#define MORELLO_RELOC_PERM_READ_ONLY  0x1ULL
#define MORELLO_RELOC_PERM_READ_WRITE 0x2ULL
#define MORELLO_RELOC_PERM_EXECUTABLE 0x4ULL

// The format of the fragments as specified by the document:
// Morello Supplement to ELF for the Arm 64-bit Architecture (AArch64)
struct morello_reloc_fragm_rel {
  ptraddr_t address;
  size_t length : 56;
  uint64_t perms : 8;
};

struct morello_reloc_fragm_tlsdesc {
  uint8_t padding[24];
  size_t length;
};

struct morello_reloc_fragm_tprel {
  uint8_t padding[8];
  size_t length;
};

#endif /* _MORELLO_ELF_MACHDEP_H_ */
