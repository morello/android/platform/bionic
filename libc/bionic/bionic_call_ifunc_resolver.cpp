/*
 * Copyright (C) 2019 The Android Open Source Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "private/bionic_call_ifunc_resolver.h"
#include <sys/auxv.h>
#include <sys/ifunc.h>

#include "private/bionic_auxv.h"
#include "private/KernelArgumentBlock.h"

// This code is called in the linker before it has been relocated, so minimize calls into other
// parts of Bionic. In particular, we won't ever have two ifunc resolvers called concurrently, so
// initializing the ifunc resolver argument doesn't need to be thread-safe.

ifunc_t __bionic_call_ifunc_resolver(ifunc_resolver_t resolver,
                                     const KernelArgumentBlock* args) {
#if defined(__aarch64__)
  static __ifunc_arg_t arg;
  static bool initialized = false;
  if (!initialized) {
    initialized = true;
    arg._size = sizeof(__ifunc_arg_t);
    arg._hwcap = args != nullptr ? args->getauxval(AT_HWCAP) : getauxval(AT_HWCAP);
    arg._hwcap2 = args != nullptr ? args->getauxval(AT_HWCAP2) : getauxval(AT_HWCAP2);
  }
  return resolver(arg._hwcap | _IFUNC_ARG_HWCAP, &arg);
#elif defined(__arm__)
  static unsigned long hwcap;
  static bool initialized = false;
  if (!initialized) {
    initialized = true;
    hwcap = args != nullptr ? args->getauxval(AT_HWCAP) : getauxval(AT_HWCAP);
  }
  return resolver(hwcap);
#else
  (void) args;
  return resolver();
#endif
}
