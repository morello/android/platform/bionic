/*
 * Copyright (C) 2022 Arm Limited
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "linker_phdr_entry_list.h"

#include <new>

static PhdrEntryList::Allocator g_phdr_entry_list_allocator;

void* PhdrEntryList::operator new(size_t count) noexcept {
  if (count != sizeof(PhdrEntryList)) {
    return nullptr;
  }

  return g_phdr_entry_list_allocator.alloc();
}

void PhdrEntryList::operator delete(void* ptr) noexcept {
  if (ptr == nullptr) {
    return;
  }

  PhdrEntryList *entry = reinterpret_cast<PhdrEntryList*>(ptr);

  g_phdr_entry_list_allocator.free(entry);
}

void PhdrEntryList::Add(const ElfW(Phdr)* phdr_entry) {
  if (phdr_entry_ == nullptr) {
    phdr_entry_ = phdr_entry;
  } else {
    PhdrEntryList *new_entry = new PhdrEntryList(phdr_entry, next_);

    next_ = new_entry;
  }
}

const ElfW(Phdr)* PhdrEntryList::Get() const {
  return phdr_entry_;
}

const PhdrEntryList* PhdrEntryList::Next() const {
  return next_;
}

PhdrEntryList::~PhdrEntryList() {
  while (next_ != nullptr) {
    PhdrEntryList *to_delete = next_;

    PhdrEntryList *after_next = to_delete->next_;
    to_delete->next_ = nullptr;

    delete to_delete;

    next_ = after_next;
  }
}
