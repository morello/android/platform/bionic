/*
 * Copyright (C) 2019 The Android Open Source Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#pragma once

#include <stdlib.h>

#include "private/bionic_elf_tls.h"

struct TlsDynamicResolverArg {
  size_t generation;
  TlsIndex index;
#ifdef __CHERI_PURE_CAPABILITY__
  size_t length;
#endif /* __CHERI_PURE_CAPABILITY__ */
};

#ifdef __CHERI_PURE_CAPABILITY__
// Used in tlsdesc_resolver_static and tlsdesc_resolver_unresolved_weak
struct TlsStaticResolverArg {
  size_t offset;  /* offset of the symbol from the thread pointer */
  size_t length;  /* symbol size */
};

struct TpRelDescriptor {
  size_t offset;  /* offset of the symbol from the thread pointer */
  size_t length;  /* symbol size */
};
#endif /* __CHERI_PURE_CAPABILITY__ */

struct TlsModule;
struct soinfo;

void linker_setup_exe_static_tls(const char* progname);
void linker_finalize_static_tls();

void register_soinfo_tls(soinfo* si);
void unregister_soinfo_tls(soinfo* si);

const TlsModule& get_tls_module(size_t module_id);

#ifdef __CHERI_PURE_CAPABILITY__
typedef uintptr_t TlsDescResolverFunc(struct TlsDescriptor*);
#else /* !__CHERI_PURE_CAPABILITY__ */
typedef size_t TlsDescResolverFunc(size_t);
#endif /* !__CHERI_PURE_CAPABILITY__ */

struct TlsDescriptor {
#ifdef __CHERI_PURE_CAPABILITY__
  TlsDescResolverFunc* func;
  union {
    TlsStaticResolverArg arg_val;
    TlsDynamicResolverArg* arg_ptr;
  } arg;

#elif defined(__arm__)
  size_t arg;
  TlsDescResolverFunc* func;
#else
  TlsDescResolverFunc* func;
  size_t arg;
#endif
};

#ifdef __CHERI_PURE_CAPABILITY__
__LIBC_HIDDEN__ extern "C" uintptr_t
tlsdesc_resolver_static(struct TlsDescriptor*);

__LIBC_HIDDEN__ extern "C" uintptr_t
tlsdesc_resolver_dynamic(struct TlsDescriptor*);

__LIBC_HIDDEN__ extern "C" uintptr_t
tlsdesc_resolver_unresolved_weak(struct TlsDescriptor*);
#else /* !__CHERI_PURE_CAPABILITY__ */
__LIBC_HIDDEN__ extern "C" size_t tlsdesc_resolver_static(size_t);
__LIBC_HIDDEN__ extern "C" size_t tlsdesc_resolver_dynamic(size_t);
__LIBC_HIDDEN__ extern "C" size_t tlsdesc_resolver_unresolved_weak(size_t);
#endif /* !__CHERI_PURE_CAPABILITY__ */
