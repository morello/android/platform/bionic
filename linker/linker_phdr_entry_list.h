/*
 * Copyright (C) 2022 Arm Limited
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#pragma once

#include <link.h>

#include "linker_block_allocator.h"

class PhdrEntryList {
 public:
  static void* operator new(size_t) noexcept;
  static void operator delete(void*) noexcept;

  PhdrEntryList()
      : phdr_entry_(nullptr), next_(nullptr) {}
  PhdrEntryList(const ElfW(Phdr*) phdr)
      : phdr_entry_(phdr), next_(nullptr) {}
  operator bool() const { return phdr_entry_ != nullptr; }
  ~PhdrEntryList();
  void Add(const ElfW(Phdr)*);
  const ElfW(Phdr)* Get() const;
  const PhdrEntryList* Next() const;

  typedef LinkerTypeAllocator<PhdrEntryList> Allocator;

 private:
  PhdrEntryList(const ElfW(Phdr*) phdr, PhdrEntryList *next)
      : phdr_entry_(phdr), next_(next) {}

  const ElfW(Phdr)* phdr_entry_;
  PhdrEntryList* next_;
};
