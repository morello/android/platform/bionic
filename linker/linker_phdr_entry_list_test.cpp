/*
 * Copyright (C) 2022 Arm Limited
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <gtest/gtest.h>

#include "linker_phdr_entry_list.h"

TEST(phdr_entry_list, empty) {
  PhdrEntryList list;

  ASSERT_EQ(nullptr, list.Get());
  ASSERT_EQ(nullptr, list.Next());
  ASSERT_EQ(false, list);
}

TEST(phdr_entry_list, single_entry1) {
  PhdrEntryList list;

  ElfW(Phdr) entry;

  list.Add((const ElfW(Phdr)*) &entry);

  ASSERT_EQ(&entry, list.Get());
  ASSERT_EQ(nullptr, list.Next());
  ASSERT_EQ(true, list);
}

TEST(phdr_entry_list, single_entry2) {
  ElfW(Phdr) entry;

  PhdrEntryList* list = new PhdrEntryList(&entry);

  ASSERT_EQ(&entry, list->Get());
  ASSERT_EQ(nullptr, list->Next());
  ASSERT_EQ(true, *list);

  delete list;
}

TEST(phdr_entry_list, multiple_entries1) {
  PhdrEntryList list;

  ElfW(Phdr) entry1, entry2, entry3;
  const PhdrEntryList *next1, *next2;

  list.Add((const ElfW(Phdr)*) &entry1);
  list.Add((const ElfW(Phdr)*) &entry2);

  ASSERT_EQ(&entry1, list.Get());
  ASSERT_EQ(true, list);

  next1 = list.Next();
  ASSERT_NE(nullptr, next1);

  ASSERT_EQ(&entry2, next1->Get());
  ASSERT_EQ(nullptr, next1->Next());
  ASSERT_EQ(true, *next1);

  list.Add((const ElfW(Phdr)*) &entry3);

  next1 = list.Next();
  ASSERT_NE(nullptr, next1);

  next2 = next1->Next();
  ASSERT_EQ(nullptr, next2->Next());

  ASSERT_EQ(&entry1, list.Get());
  ASSERT_EQ(true,
            next1->Get() == &entry2 && next2->Get() == &entry3
            || next2->Get() == &entry2 && next1->Get() == &entry3);
  ASSERT_EQ(true, list);
  ASSERT_EQ(true, *next1);
  ASSERT_EQ(true, *next2);
}

TEST(phdr_entry_list, multiple_entries2) {
  PhdrEntryList list;

  constexpr size_t entries_num = 20;
  ElfW(Phdr) entries[entries_num];

  for (size_t i = 0; i < entries_num; i++) {
    list.Add((const ElfW(Phdr)*) &entries[i]);

    std::vector<const ElfW(Phdr)*> found_entries;

    const PhdrEntryList *iter = &list;

    while (iter != nullptr && *iter) {
      found_entries.push_back(iter->Get());

      iter = iter->Next();
    }

    std::sort(found_entries.begin(), found_entries.end());
    for (size_t j = 0; j < i; j++) {
      ASSERT_EQ(&entries[j], found_entries[j]);
    }
  }
}
